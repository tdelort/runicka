﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] GameObject[] enemy;
    [SerializeField] float radius = 30f;
    [SerializeField] private ScoreManager nbVagues;
    [SerializeField] private PopUpManager popUpFin;

    int wave = 0;

    int mobs = 0;

    int mobsLeft = 0;

    float time = 0;
    float mobTime = 0;

    int nbMobsOnMap = 0;

    private void Start()
    {
        StartCoroutine(UpdateMobs());   
    }

    IEnumerator UpdateMobs()
    {
        while(true)
        {
            //OUCH, better not call that line every frames, instead once every seconds is fine
            nbMobsOnMap = GameObject.FindGameObjectsWithTag("Enemy").Length;
            yield return new WaitForSeconds(1f);
        }
    }

    bool reset = true;

    void Update()
    {
        Debug.Log("TIMES : "  + time);
        if (nbMobsOnMap <= 0 && mobsLeft <= 0 && !reset)
        {
            time = 0f;
            reset = true;
        }

        if (wave <= 5)
        {
            if (nbMobsOnMap == 0 && mobsLeft == 0 && time > 5)
            {
                if (wave >= 1)
                {
                    nbVagues.score += 1;
                }
                wave += 1;
                mobs += ( (wave%2==0) ? 3 : 2);
                mobsLeft = mobs;
                time = 0;
                reset = false;

            }

            if (mobsLeft > 0 && mobTime > 0.1f)
            {
                var newEnemy = Instantiate(enemy[Random.Range(0,4)], GetSpawnLocation(), transform.rotation);
                newEnemy.gameObject.SetActive(true);
                mobsLeft -= 1;
                time = 0;
            }
        }
        else
        {
            popUpFin.gameObject.SetActive(true);
            time = 0;
        }
        time += Time.deltaTime;
        mobTime += Time.deltaTime;
    }

    Vector3 GetSpawnLocation()
    {
        //Random position on circle x² + y² = r²
        Vector2 pos = Random.insideUnitCircle.normalized;
        pos = (pos == Vector2.zero ? Vector2.right : pos); //protecting against edge case
        pos *= radius;
        return new Vector3(pos.x, 0, pos.y);
    }
}
