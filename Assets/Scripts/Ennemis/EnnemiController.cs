﻿using UnityEngine;
using UnityEngine.AI;

public class EnnemiController : MonoBehaviour
{
    //attackHitBox
    public GameObject attackBox;

    //stats
    public int health;
    public float speed;
    public Element elemType;

    //movement
    NavMeshAgent agent;
    GameObject player;

    //attacking
    public float timeBetweenAttacks;
    public float attackRange;
    bool alreadyAttacked;
    bool playerInRange;
    

    //masks
    public LayerMask whatIsGround, whatIsPlayer;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("Player");
        agent.speed = speed;
    }

    void Update()
    {
        //check if player in attack range
        playerInRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (!playerInRange) { ChasePlayer(); }
        else AttackPlayer();     
    }

    private void AttackPlayer()
    {
        agent.SetDestination(transform.position);
        transform.LookAt(player.transform);

        if (!alreadyAttacked)
        {
            Attack();

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("touché");
        if (other.gameObject.CompareTag("Ball"))
        {
            Debug.Log("touché");
            Projectile ball = other.gameObject.GetComponent<Projectile>();
            Element weakness = WeaknessElement(elemType);
            if (ball.type == weakness)
                TakeDamage(5);  //à traiter en fonction du type ? voir un ball.damage
            Destroy(ball);
        }
    }
    
    public static Element WeaknessElement(Element elem)
    {
        switch(elem)
        {
            case Element.FIRE:
                return Element.WATER;
            case Element.WATER:
                return Element.EARTH;
            case Element.EARTH:
                return Element.AIR;
            case Element.AIR:
                return Element.FIRE;
            default:
                return Element.AIR; //never happens
        }
    }
    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
            Invoke(nameof(DestroyEnemy), 0.1f);
    }

    private void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    public virtual void Attack()
    {
        //overrided in specific enemy behaviour
    }

    public void dealDamage(int damage)
    {
       player.GetComponent<PlayerBehaviour>().TakeDamage(damage);
    }
}
