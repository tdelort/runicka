﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] PlayerBehaviour pb;
    [SerializeField] Slider lifeBar;

    void Start()
    {
        lifeBar.maxValue = PlayerBehaviour.maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        lifeBar.value = pb.GetHealth();   
    }
}
