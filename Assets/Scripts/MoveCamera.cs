﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    [SerializeField]
    Transform target;
    [SerializeField]
    Vector3 offset = new Vector3(0, 8, -8);

    void Update()
    {
        transform.position = target.position + offset;
    }
}
