﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterShield : MonoBehaviour
{
    public float shieldTime;
    float remainingShieldTime;

    private void Update()
    {
        if (gameObject.active)
            remainingShieldTime -= Time.deltaTime;
        if (remainingShieldTime < 0)
            gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        remainingShieldTime = shieldTime;
    }
}
