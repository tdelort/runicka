﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] Animator mageAnimator = null;


    Camera cam;

    NavMeshAgent agent;

    LineRenderer line;

    private void Start()
    {
        cam = GameObject.Find("Main Camera").gameObject.GetComponent<Camera>();
        agent = GetComponent<NavMeshAgent>();
        line = GetComponentInChildren<LineRenderer>();
    }

    private void Update()
    {
        
        //Raycasting
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            //Move towards target if click
            if (Input.GetMouseButtonDown(0))
            {
                agent.SetDestination(hit.point);
            }

            //Adjust casting direction
            line.transform.LookAt(new Vector3(hit.point.x,line.transform.position.y,hit.point.z));
        }

        //Rotate in the direction we are traveling
        if (GetComponent<Rigidbody>().velocity.magnitude != 0)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation,Quaternion.LookRotation(GetComponent<Rigidbody>().velocity), 15);
        }
        UpdateAnimator();
    }


    private void UpdateAnimator()
    {
        mageAnimator.SetFloat("velocity", agent.velocity.magnitude / agent.speed);
        mageAnimator.SetBool("isRunning", agent.velocity.sqrMagnitude != 0f);
        //mageAnimator.SetBool("Cast", isShooting);
    }
}
