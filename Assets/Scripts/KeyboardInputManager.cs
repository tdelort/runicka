﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KeyboardInputManager : MonoBehaviour
{
    public static int KEYBOARD_WIDTH = 10;
    public static int KEYBOARD_HEIGHT = 4;
    public static Dictionary<KeyCode, (int, int)> keyPositions = new Dictionary<KeyCode, (int, int)>()
    {
        {KeyCode.Alpha1, (0,3)}, {KeyCode.Alpha2, (1,3)}, {KeyCode.Alpha3, (2,3)}, {KeyCode.Alpha4, (3,3)}, {KeyCode.Alpha5, (4,3)}, {KeyCode.Alpha6, (5,3)}, {KeyCode.Alpha7, (6,3)}, {KeyCode.Alpha8, (7,3)}, {KeyCode.Alpha9, (8,3)}, {KeyCode.Alpha0, (9,3)}, 
        {KeyCode.A ,(0,2)}, {KeyCode.Z, (1,2)}, {KeyCode.E ,(2,2)}, {KeyCode.R ,(3,2)}, {KeyCode.T ,(4,2)}, {KeyCode.Y ,(5,2)}, {KeyCode.U ,(6,2)}, {KeyCode.I ,(7,2)}, {KeyCode.O ,(8,2)}, {KeyCode.P ,(9,2)}, 
        {KeyCode.Q ,(0,1)}, {KeyCode.S, (1,1)}, {KeyCode.D ,(2,1)}, {KeyCode.F ,(3,1)}, {KeyCode.G ,(4,1)}, {KeyCode.H ,(5,1)}, {KeyCode.J ,(6,1)}, {KeyCode.K ,(7,1)}, {KeyCode.L ,(8,1)}, {KeyCode.M ,(9,1)}, 
        {KeyCode.W ,(0,0)}, {KeyCode.X, (1,0)}, {KeyCode.C ,(2,0)}, {KeyCode.V ,(3,0)}, {KeyCode.B ,(4,0)}, {KeyCode.N ,(5,0)}, {KeyCode.Comma ,(6,0)}, {KeyCode.Period ,(7,0)}, {KeyCode.Slash ,(8,0)}, {KeyCode.BackQuote ,(9,0)}, 
    }; //BackQuotes is actually the key with § and ! 


    float strokeTimerThreshold = 0.3f; //Time between each press of a stroke
    float strokeTimer = 0f;
    bool analyzed = true;

    List<(int, int)> strokeData = new List<(int, int)>();
    public Vector2 direction;

    void Awake()
    {
        //For the gui part
        _staticRectTexture = new Texture2D(1, 1);
        _staticRectStyle = new GUIStyle();
    }

    private void Update()
    {
        bool keyPressedThisFrame = false;
        foreach(KeyCode key in keyPositions.Keys)
        {
            if(Input.GetKeyDown(key))
            {
                keyPressedThisFrame = true;
                analyzed = false;
                (int, int) pos;
                if(keyPositions.TryGetValue(key,out pos))
                {
                    //Debug.Log(key + " pressed is at " + pos);
                    strokeData.Add(pos);
                    Debug.Log("Number of keys in stroke : " + strokeData.Count);
                    mageAnimator.SetBool("isCasting", true);
                    agent.SetDestination(agent.transform.position);
                }
            }
        }

        //updating timer
        strokeTimer = keyPressedThisFrame ? 0f : strokeTimer + Time.deltaTime;

        //End of stroke reached
        if(strokeTimer > strokeTimerThreshold && !analyzed)
        {
            //Use strokeData
            AnalyseStroke();
            strokeData = new List<(int, int)>();
            analyzed = true;
            //end of Casting
            //Please don't look at this too much, it's ugly but it works just fine :D
            string hash = direction.x + "_" + direction.y;
            switch(hash)
            {
                case "0_1":
                    Shoot(Element.AIR);
                    break;
                case "1_1":
                    StartCoroutine(Shield(Element.AIR));
                    break;
                case "1_0":
                    Shoot(Element.FIRE);
                    break;
                case "1_-1":
                    StartCoroutine(Shield(Element.FIRE));
                    break;
                case "0_-1":
                    Shoot(Element.EARTH);
                    break;
                case "-1_-1":
                    StartCoroutine(Shield(Element.EARTH));
                    break;
                case "-1_0":
                    Shoot(Element.WATER);
                    break;
                case "-1_1":
                    StartCoroutine(Shield(Element.WATER));
                    break;
                default:
                    break;
            }
            mageAnimator.SetBool("isCasting", false);
        }
    }

    void AnalyseStroke()
    {
        if (strokeData.Count < 2)
            return;

        Debug.Log("Analyzing Stroke");
        (int, int) first = strokeData[0];
        (int, int) last = strokeData[strokeData.Count - 1];
        int dx = last.Item1 - first.Item1;
        int dy = last.Item2 - first.Item2;
        //Debug.Log("dx : " + (dx/2) + " dy : " + (dy/2));
        direction = new Vector2(Math.Sign(dx/2),Math.Sign(dy/2)); // divided by to to reduce the resolution
        //direction contains the direction of the stroke (it can be any of the 9 directions possibles)
        Debug.Log("Stroke Direction : " + direction);
    }   

    //GUI part
    [SerializeField]
    public Vector2 size = new Vector2(5, 5);
    [SerializeField]
    public Vector2 margin = new Vector2(1, 1);

    [SerializeField]
    public Color pressedColor = Color.red;
    [SerializeField]
    public Color idleColor = Color.white;

    private static Texture2D _staticRectTexture;
    private static GUIStyle _staticRectStyle;

    public static void GUIDrawRect(Rect position, Color color)
    {
        _staticRectTexture.SetPixel(0, 0, color);
        _staticRectTexture.Apply();

        _staticRectStyle.normal.background = _staticRectTexture;

        GUI.Box(position, GUIContent.none, _staticRectStyle);
    }

    private void OnGUI()
    {
        for(int y = 0; y < KEYBOARD_HEIGHT; y++)
        {
            for(int x = 0; x < KEYBOARD_WIDTH; x++)
            {
                bool isInStroke = strokeData.Contains((x, y));
                Vector2Int it = new Vector2Int(x, KEYBOARD_HEIGHT - y - 1);
                GUIDrawRect(new Rect(margin + it*(size+margin) + Vector2.right * it.y * margin, size), isInStroke ? pressedColor : idleColor);
            }
        }
    }


    //Action part
    [SerializeField]
    NavMeshAgent agent;
    [SerializeField]
    Transform lineTransform;
    [SerializeField]
    Animator mageAnimator;
    [SerializeField]
    GameObject fireBall;
    [SerializeField]
    GameObject waterBall;
    [SerializeField]
    GameObject earthBall;
    [SerializeField]
    GameObject airBall;
    [SerializeField]
    PlayerBehaviour player;

    private void Shoot(Element elem)
    {
        Debug.Log("SHOOT");
        agent.SetDestination(agent.transform.position);
        GameObject newBall;
        switch(elem)
        {
            case Element.WATER:
                newBall = Instantiate(waterBall, agent.transform.position, lineTransform.rotation);
                break;
            case Element.EARTH:
                newBall = Instantiate(earthBall, agent.transform.position, lineTransform.rotation);
                break;
            case Element.FIRE:
                newBall = Instantiate(fireBall, agent.transform.position, lineTransform.rotation);
                break;
            case Element.AIR:
                newBall = Instantiate(airBall, agent.transform.position, lineTransform.rotation);
                break;
            default:
                break;
        }
    }

    private IEnumerator Shield(Element elem)
    {
        if (!player.GetShielded())
        {
            Debug.Log("SHIELD");
            player.SetShielded(true, elem);
            GameObject shield;
            switch(elem)
            {
                case Element.WATER:
                    shield = player.gameObject.transform.Find("WaterShield").gameObject;
                    break;
                case Element.EARTH:
                    shield = player.gameObject.transform.Find("EarthShield").gameObject;
                    break;
                case Element.FIRE:
                    shield = player.gameObject.transform.Find("FireShield").gameObject;
                    break;
                case Element.AIR:
                    shield = player.gameObject.transform.Find("AirShield").gameObject;
                    break;
                default:
                    shield = player.gameObject.transform.Find("WaterShield").gameObject;
                    break;
            }
            shield.SetActive(true);
            yield return new WaitForSeconds(3f);
            shield.SetActive(false);
            player.SetShielded(false, elem);
        }
    }

}

