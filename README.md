# Runicka

[Project Wiki](https://barzilouik.space/carnet/doku.php?id=pub:siana2021:abcd_aristide_auphan_basile_bonicel_paul_cathrine_tristan_delort)

[Project Trello](https://trello.com/b/BS6hRf3R/coonif%C3%A8res)

> Dans le monde de Runicka, les sorts sont transmis entre magicien de génération en génération à travers des cribles. Ces cribles où sont représentés des Runes, qui permettent de canaliser la magie lorsqu'elle sont dessinées, ont malheureusement été perdues au fil des années. Mais une grand menace planant sur le monde vous pousse à les rassembler. C'est là que vous rentrez en jeu !! Munissez vous de votre clavier AZERTY (le support QWERTY sera sûrement considéré) et dessinez/lancez vos sorts à tout va !
