﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemiBehaviour : EnnemiController
{
    public override void Attack()
    {
        attackBox.SetActive(true);
        Invoke(nameof(stopAttack), 1);
    }

    private void stopAttack()
    {
        attackBox.SetActive(false);
    }
}
