﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Element
{
    WATER,
    EARTH,
    FIRE,
    AIR,
}

public class PlayerBehaviour : MonoBehaviour
{
    public static float maxHealth = 50f;
    float health = maxHealth;
    bool shielded = false;
    Element shieldElement;

    public PopUpManager popUpFinal;
    public Canvas playerUI;

    public float GetHealth()
    {
        return health;
    }

    public void SetShielded(bool shielded, Element element)
    {
        this.shielded = shielded;
        this.shieldElement = element;
    }

    public bool GetShielded()
    {
        return shielded;
    }

 
    private void OnTriggerEnter(Collider collision)
    {
        //Compare tag to check element of attack an deduces damages
        //If shielded then cancel damage and if shielded && elemOfShield == enemyWeakness then it rebounds and kill enemy
        if (collision.gameObject.CompareTag("Attack"))
        {
            EnnemiBehaviour eB = collision.gameObject.GetComponentInParent<EnnemiBehaviour>();
            Element enemyType = eB.elemType;
            if (shielded)
            {
                Element enemyWeakness = EnnemiController.WeaknessElement(enemyType);
                if (shieldElement == enemyWeakness)
                    eB.TakeDamage(5);
                    Debug.Log("you successfully paried the attack");
                return;
            }

            TakeDamage(5);
        }
    }

    public void TakeDamage(int damage)
    {
        if (health > damage)
        {
            health -= damage;
            Debug.Log("you take damage OOF");
        }
        else
        {
            health = 0;
            //death animation
            playerUI.enabled = false;
            popUpFinal.gameObject.SetActive(true);

            Debug.Log("you dead");
        }
    }

    private void DisableShields()
    {
        gameObject.transform.Find("AirShield").gameObject.SetActive(false);
        gameObject.transform.Find("WaterShield").gameObject.SetActive(false);
        gameObject.transform.Find("FireShield").gameObject.SetActive(false);
        gameObject.transform.Find("EarthShield").gameObject.SetActive(false);
    }
}
