# Runicka DevLog

## First Sprint

### InputManager

The main thing we needed to do is the InputManager since it will define the whole project. 
At first, we just used a 4x4 matrix of keys (on an AZERTY Keyboard, it was 1234 AZER QSDF WXCV) and from this example we moved on to a 10 by 4 matrix with all the keys under the AlphaNumeric numbers (hence 1234567890 AZERTYUIOP QSDFGHJKLM WXCVBN?./§)  

Then we just stored Inputs on a 2D array buffer that would be emptied after a certain time without inputs from the matrix. We analysed the buffer and the first key that was pressed from this buffer to get a "gesture".

### Graphics

We also needed to settle on a Graphics style as soon as possible to get the 3D Models done. We decided to go the lowpoly celshaded way to get a satisfying result in the short amount of time we had.

## Second Sprint

In this sprint, we mostly made assets for the game.

### Models

We made the 3D Models for the Player and the arena in Blender, and the animations with MixAmo. Then they we exported them as .fbx to be added to the game.

### Music

We also made a tune for the game to make it less silent and more fun.

## Third Sprint

### Navigation

In this sprint, we added Player and Enemy movement using a NavMesh. This was really helpful because it meant we could use pathfinding and thus add obstacles in our arena. We settled on movement system using the mouse to free the left hand and the keyboard for *Drawing*.

### Spells

We also decided which spells we needed to implement and to which *gesture* it would be linked. We chose to use the 8 direction and to use 4 elements and 2 spells. An Attack spell and a shield. This choice was made to ensure all the spell would be used and not just one or two since you need to use the right element on each enemy.

### Shaders

The default look of Unity is not that pretty with simple assets (we aren't 3D artists). So an easy way to make the game look better is to use less details. When you look at The Legend of Zelda: The Wind Waker, even though the game is almost 20 years old it doesn't look ugly. The trick is celshading. It mimicks the look of cartoons by using non linear shadows and a cartoon outline.
We added the non-soft shadows and changed color so that they are flat to hide a bit of details and voila ! The game now looks a bit better !

## Final Sprint

In this sprint, we needed to put all of our work together to make the game. We added the player in the arena and made sure the spells were working along with the animations. We then added the enemies and all the damages/health/attacks systems. Finally, we needed to add some UI or the user experience would have been really bad. So we made an health bar and an enemy-wave-counter in the main scene, a Launch menu, a death/win pop-up, and a small tutorial.