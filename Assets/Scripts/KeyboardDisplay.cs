﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardDisplay : MonoBehaviour
{
    [SerializeField]
    public Vector2 size = new Vector2(5, 5);
    [SerializeField]
    public Vector2 margin = new Vector2(1, 1);

    [SerializeField]
    public Color pressedColor;
    [SerializeField]
    public Color idleColor;

    private bool[,] use = new bool[KeyboardInputManager.KEYBOARD_WIDTH, KeyboardInputManager.KEYBOARD_HEIGHT];


    private static Texture2D _staticRectTexture;
    private static GUIStyle _staticRectStyle;
    void Awake()
    {
        _staticRectTexture = new Texture2D(1, 1);
        _staticRectStyle = new GUIStyle();
    }

    private void Update()
    {
        foreach(KeyCode key in KeyboardInputManager.keyPositions.Keys)
        {
            if(Input.GetKey(key))
            {
                (int, int) pos;
                if(KeyboardInputManager.keyPositions.TryGetValue(key,out pos))
                {
                    use[pos.Item1, pos.Item2] = true;
                }
            }
            else
            {
                (int, int) pos;
                if(KeyboardInputManager.keyPositions.TryGetValue(key,out pos))
                {
                    use[pos.Item1, pos.Item2] = false;
                }
            }
        }
    }

    public static void GUIDrawRect(Rect position, Color color)
    {
        _staticRectTexture.SetPixel(0, 0, color);
        _staticRectTexture.Apply();

        _staticRectStyle.normal.background = _staticRectTexture;

        GUI.Box(position, GUIContent.none, _staticRectStyle);
    }

    private void OnGUI()
    {
        for(int y = 0; y < KeyboardInputManager.KEYBOARD_HEIGHT; y++)
        {
            for(int x = 0; x < KeyboardInputManager.KEYBOARD_WIDTH; x++)
            {
                Vector2Int it = new Vector2Int(x, KeyboardInputManager.KEYBOARD_HEIGHT - y - 1);
                GUIDrawRect(new Rect(margin + it*(size+margin) + Vector2.right * it.y * margin, size), use[x, y] ? pressedColor : idleColor);
            }
        }
    }
}
