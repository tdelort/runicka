﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/ToonShader"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
        _MainTex("Albedo", 2D) = "white" {}
    }
	SubShader
    {
        Tags {"RenderType"="Opaque" }
        LOD 100

        Pass {
            CGPROGRAM
            #pragma vertex vert alpha
            #pragma fragment frag alpha
            #pragma target 2.0
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                half3 worldNormal : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST, _Color;

            float tooning(float intensity)
            {
                float tI = 0;
                if (intensity < 0.1)
                    tI = 0.1;
                else if (intensity < 0.3)
                    tI = 0.3;
                else if (intensity < 0.6)
                    tI = 0.5;
                else if (intensity < 0.85)
                    tI = 0.7;
                else
                    tI = 1.0;
                return tI;
            }

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                // sample the texture
                float3 light = float3(0,1,0);
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;
				float intensity = dot(normalize(light), i.worldNormal);
                return saturate(col * (tooning(intensity) + 0.2));
            }
            ENDCG
		}
        /*
        Pass {
			Cull front
			Tags {
				"LightMode" = "ForwardBase"
			}

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase

			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#include "Lighting.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float3 normal: NORMAL;
			};

			struct v2f {
				float4 position : SV_POSITION;
			};

			v2f vert(appdata v) {
				v2f o;
				float4 position = UnityObjectToClipPos(v.vertex);
				o.position = position + 0.005f * UnityObjectToClipPos(normalize(v.normal));
				return o;
			}

			fixed4 _Color2;

			fixed4 frag(v2f i) : SV_Target {
				fixed4 col = _Color2;
				col.a = 1.0;
				return col;
			}
			ENDCG
		}
        */
    }
}
